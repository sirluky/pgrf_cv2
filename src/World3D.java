import objectdata.*;
import objectops.RenderLineList;
import rasterdata.RasterImage;
import rasterops.Liner;
import transforms.*;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class World3D {

    final Point2D[] mouseLook = {null, null};
    Vec3D cameraPosition = new Vec3D(7, 9, 6);
    Camera camera = null;
    private Liner liner;
    private RasterImage raster;
    private Canvas canvas;
    private RasterImage img;
    private JPanel panel;
    private Scene scene = new Scene();
    private RenderLineList renderer = null;

    public World3D(RasterImage img, Liner liner, Canvas canvas, JPanel panel) {
        this.raster = img;
        this.liner = liner;
        this.canvas = canvas;
        this.panel = panel;
        this.img = img;

        init();
    }

    public void init() {
        camera = new Camera()
                .withPosition(cameraPosition)
                .withAzimuth(canvas.azimuthToOrigin(cameraPosition))
                .withZenith(canvas.zenithToOrigin(cameraPosition));

        renderer = new RenderLineList(camera, raster, liner);

        // create objects and position them in the scene
        scene.addSolid(new Axis(), new Mat4Transl(0, 0, 0).mul(new Mat4Scale(5)));
        scene.addSolid(new Cube(), new Mat4Transl(-0, 0, -0).mul(new Mat4Scale(3)));
        scene.addSolid(new Pyramid(), new Mat4Transl(4, 0, 1).mul(new Mat4Scale(2)));
        scene.addSolid(new OctaHedron(), new Mat4Transl(-3, 0, 2).mul(new Mat4Scale(2)));
        scene.addSolid(new Cuboid(), new Mat4Transl(0, 3, 0).mul(new Mat4Scale(2)));
        Mat4 cubeTransform = scene.getModelMats().get(0);
        scene.getModelMats().set(0, cubeTransform);
        Point3D[] curvePoints = {
                new Point3D(50, 400, 0),
                new Point3D(100, 400, 0),
                new Point3D(150, 500, 0),
                new Point3D(200, 200, 0),
                new Point3D(250, 500, 0),
                new Point3D(300, 400, 0),
                new Point3D(350, 300, 0),
                new Point3D(400, 400, 0),
                new Point3D(450, 460, 0),
                new Point3D(500, 530, 0),
                new Point3D(550, 390, 0),
                new Point3D(600, 500, 0),
                new Point3D(650, 100, 0),
                new Point3D(700, 400, 0),
                new Point3D(750, 100, 0),
                new Point3D(500, 50, 0)
        };
        double scale = 0.01;
        List<Point3D> pointsList = Arrays.stream(curvePoints)
                .map(point3D -> new Point3D(point3D.getX() * scale, point3D.getY() * scale, point3D.getZ() * scale))
                .collect(Collectors.toList());


        scene.addSolid(new Cube(), new Mat4Transl(1, 0, 0).mul(new Mat4Scale(1)));


        Point3D[] bezier = new Point3D[]{
                new Point3D(-1, -1, 2),
                new Point3D(-0.5, -1, -1),
                new Point3D(0.5, -1, -2),
                new Point3D(1, -1, 1),
                new Point3D(-1, -0.5, -1),
                new Point3D(-0.5, -0.5, 2),
                new Point3D(0.5, -0.5, -1),
                new Point3D(1, -0.5, -2),
                new Point3D(-1, 0.5, 2),
                new Point3D(-0.5, 0.5, -2),
                new Point3D(0.5, 0.5, 1),
                new Point3D(1, 0.5, -1),
                new Point3D(-1, 1, -2),
                new Point3D(-0.5, 1, 1),
                new Point3D(0.5, 1, 2),
                new Point3D(1, 1, -1)
        };
        scene.addSolid(new Surface(Cubic.BEZIER, bezier, 10), new Mat4RotZ(Math.PI).mul(new Mat4Transl(2, -4, 3)).mul(new Mat4Scale(2)));

        // blank object just to have something selected by defaullt
        scene.addSolid(new Cube(), new Mat4Transl(0, 0, 0).mul(new Mat4Scale(0)));

        // TODO
        // do animation

        // run draw method to let me see it
        draw();
    }


    public void update() {
        List<Mat4> modelmats = scene.getModelMats();
        modelmats.set(1, modelmats.get(1).mul(new Mat4Rot(Math.toRadians(0.5), 0, 0.2, 0)));

        draw();
    }

    public void draw() {
        canvas.clear();
        renderer.renderScene(scene, camera.getViewMatrix(), new Mat4PerspRH(Math.PI / 2, 1, 0.1, 200), liner, img, 0xffffff, 0xff8800);

        canvas.present();
    }

    public void move(char key) {
        switch (key) {
            case 'W':
                camera = camera.forward(0.1);
                break;
            case 'S':
                camera = camera.backward(0.1);
                break;
            case 'A':
                camera = camera.left(0.1);
                break;
            case 'D':
                camera = camera.right(0.1);
                break;
            case 'Q':
                camera = camera.up(0.1);
                break;
            case 'E':
                camera = camera.down(0.1);
                break;
        }
        draw();

    }

    public void mouseMove(Point2D[] mouseLook) {

        if (Arrays.stream(mouseLook).allMatch(Objects::nonNull)) {
            double mouseMoveSpeed = 0.0174533;

            double xvec = mouseLook[1].getX() - mouseLook[0].getX();
            double yvec = mouseLook[1].getY() - mouseLook[0].getY();
            xvec /= img.getWidth();
            yvec /= img.getHeight();

            camera = camera.addAzimuth(mouseMoveSpeed * -xvec);
            camera = camera.addZenith(mouseMoveSpeed * -yvec);
        }
        draw();
    }

    public void rotateObjects(char key, double speed) {
        List<Mat4> modelmats = scene.getModelMats();
        switch (key) {
            case 'X':
                if (renderer.selectedObjectIndex > 0) {
                    modelmats.set(renderer.selectedObjectIndex, modelmats.get(renderer.selectedObjectIndex).mul(new Mat4Rot(Math.toRadians(0.5), speed, 0, 0)));
                } else {
                    for (int i = 1; i < modelmats.size(); i++) {
                        modelmats.set(i, modelmats.get(i).mul(new Mat4Rot(Math.toRadians(0.5), speed, 0, 0)));
                    }
                }
                break;
            case 'Y':
                if (renderer.selectedObjectIndex > 0) {
                    modelmats.set(renderer.selectedObjectIndex, modelmats.get(renderer.selectedObjectIndex).mul(new Mat4Rot(Math.toRadians(0.5), 0, speed, 0)));
                } else {
                    for (int i = 1; i < modelmats.size(); i++) {
                        modelmats.set(i, modelmats.get(i).mul(new Mat4Rot(Math.toRadians(0.5), 0, speed, 0)));
                    }
                }
                break;
            case 'Z':
                if (renderer.selectedObjectIndex > 0) {
                    modelmats.set(renderer.selectedObjectIndex, modelmats.get(renderer.selectedObjectIndex).mul(new Mat4Rot(Math.toRadians(0.5), 0, 0, speed)));
                } else {
                    for (int i = 1; i < modelmats.size(); i++) {
                        modelmats.set(i, modelmats.get(i).mul(new Mat4Rot(Math.toRadians(0.5), 0, 0, speed)));
                    }
                }
                break;
        }

        draw();
    }

    public void moveObjects(char key, double speed) {
        List<Mat4> modelmats = scene.getModelMats();
        switch (key) {
            case 'X':
                if (renderer.selectedObjectIndex > 0) {
                    modelmats.set(renderer.selectedObjectIndex, modelmats.get(renderer.selectedObjectIndex).mul(new Mat4Transl(speed, 0, 0)));
                } else {
                    for (int i = 1; i < modelmats.size(); i++) {
                        modelmats.set(i, modelmats.get(i).mul(new Mat4Transl(speed, 0, 0)));
                    }
                }

                break;
            case 'Z':
                if (renderer.selectedObjectIndex > 0) {
                    modelmats.set(renderer.selectedObjectIndex, modelmats.get(renderer.selectedObjectIndex).mul(new Mat4Transl(0, speed, 0)));
                } else {
                    for (int i = 1; i < modelmats.size(); i++) {
                        modelmats.set(i, modelmats.get(i).mul(new Mat4Transl(0, speed, 0)));
                    }
                }
                break;
            case 'Y':
                if (renderer.selectedObjectIndex > 0) {
                    modelmats.set(renderer.selectedObjectIndex, modelmats.get(renderer.selectedObjectIndex).mul(new Mat4Transl(0, 0, speed)));
                } else {
                    for (int i = 1; i < modelmats.size(); i++) {
                        modelmats.set(i, modelmats.get(i).mul(new Mat4Transl(0, 0, speed)));
                    }
                }
                break;


        }

        draw();
    }

    public void zoomObjects(double speed) {
        List<Mat4> modelmats = scene.getModelMats();
        if (renderer.selectedObjectIndex > 0) {
            modelmats.set(renderer.selectedObjectIndex, modelmats.get(renderer.selectedObjectIndex).mul(new Mat4Scale(speed, speed, speed)));
        } else {
            for (int i = 1; i < modelmats.size(); i++) {
                modelmats.set(i, modelmats.get(i).mul(new Mat4Scale(speed, speed, speed)));
            }
        }
        draw();
    }

    public void setSelectedObjectIndex(int index) {
        renderer.setSelectedObjectIndex(index);
        draw();
    }

    public void scale() {

    }

    public void add() {

    }

    public void remove() {

    }
}
