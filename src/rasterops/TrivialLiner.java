package rasterops;

import objectops.Point2D;
import rasterdata.RasterImage;

public class TrivialLiner<P> implements Liner<P>{
    @Override
    public void drawLine(RasterImage<P> img, Point2D c, transforms.Point2D r, P pixelValue) {
        drawLine(img, (int) c.getX(), (int) c.getY(), (int) r.getX(), (int) r.getY(), pixelValue);

    }

    public void drawLine(final RasterImage img, final transforms.Point2D c, final transforms.Point2D  r , final  P pixelValue) {
        drawLine(img, (int) c.getX(), (int) c.getY(), (int) r.getX(), (int) r.getY(), pixelValue);
    }

    @Override
    public void drawLine(final  RasterImage<P> img,  int c1,  int r1,
                          int c2,  int r2, final  P pixelValue) {
        // iterating over r and calculating c
        final double k = (r2 - r1) / (double) (c2 - c1);
        final double q = r1 - k * c1;


        if (Math.abs(r2 - r1) < Math.abs(c2 - c1)) { //iterate over c
            if (c1 > c2) { // swap start and end points
                int pom = c1;
                c1 = c2;
                c2 = pom;
            }
            // iterating over c and calculating r
            if(c1 == c2) {
                for (int c = r1; c <= r2; c++) {
                    img.setPixel(c1, c, pixelValue);
                }
            } else {

                for (int c = c1; c <= c2; c++) {
                    int r = (int) Math.round(k * c + q);
                    img.setPixel(c, r, pixelValue);
                }
            }

        } else { // iterate over r
            if (r1 > r2) { // swap start and end points
                int pom = r1;
                r1 = r2;
                r2 = pom;
            }


            if(c1 == c2) {
                for (int c = r1; c <= r2; c++) {
                    img.setPixel(c1, c, pixelValue);
                }
            } else {
                for (int c = r1; c <= r2; c++) {
                    int r = (int) Math.round((c - q) / k);
                    img.setPixel(r, c, pixelValue);
                }
            }
        }
    }



    public void drawLinePatterned(final  RasterImage<P> img,  int c1,  int r1,
                         int c2,  int r2) {
        // iterating over r and calculating c
        final double k = (r2 - r1) / (double) (c2 - c1);
        final double q = r1 - k * c1;


        if (Math.abs(r2 - r1) < Math.abs(c2 - c1)) { //iterate over c
            if (c1 > c2) { // swap start and end points
                int pom = c1;
                c1 = c2;
                c2 = pom;
            }
            // iterating over c and calculating r
            if(c1 == c2) {
                for (int c = r1; c <= r2; c++) {
                    img.setPixel(c1, c, (P) new ChessStyleFill().fill(c1, c));
                }
            } else {

                for (int c = c1; c <= c2; c++) {
                    int r = (int) Math.round(k * c + q);
                    img.setPixel(c, r, (P) new ChessStyleFill().fill(c,r));
                }
            }

        } else { // iterate over r
            if (r1 > r2) { // swap start and end points
                int pom = r1;
                r1 = r2;
                r2 = pom;
            }


            if(c1 == c2) {
                for (int c = r1; c <= r2; c++) {
                    img.setPixel(c1, c, (P) new ChessStyleFill().fill(c1, c));
                }
            } else {
                for (int c = r1; c <= r2; c++) {
                    int r = (int) Math.round((c - q) / k);
                    img.setPixel(r, c, (P) new ChessStyleFill().fill(c1, c));
                }
            }
        }
    }



    public void drawLineLined(final  RasterImage<P> img, int c1, int r1,
                              int c2, int r2, int dotSize, final  P pixelValue) {
        int dotCounter = 0;

        // iterating over r and calculating c
        final double k = (r2 - r1) / (double) (c2 - c1);
        final double q = r1 - k * c1;



        if (Math.abs(r2 - r1) < Math.abs(c2 - c1)) { //iterate over c
            if (c1 > c2) { // swap start and end points
                int pom = c1;
                c1 = c2;
                c2 = pom;

            }

            // iterating over c and calculating r
            if(c1 == c2) {
                for (int c = r1; c <= r2; c++) {
                    if(dotCounter < dotSize && dotCounter >= 1){
                        img.setPixel(c1, c, pixelValue);
                    }
                    if(dotCounter >= dotSize){
                        dotCounter = -dotSize;
                    }
                    dotCounter++;
                }
            } else {
                for (int c = c1; c <= c2; c++) {
                    int r = (int) Math.round(k * c + q);
                    if(dotCounter < dotSize && dotCounter >= 0){
                        img.setPixel(c, r, pixelValue);
                    }
                    if(dotCounter >= dotSize){
                        dotCounter = -dotSize;
                    }
                    dotCounter++;
                }
            }
        } else { // iterate over r
            if (r1 > r2) { // swap start and end points
                int pom = r1;
                r1 = r2;
                r2 = pom;
            }


            if(c1 == c2) {
                for (int c = r1; c <= r2; c++) {
                    if(dotCounter < dotSize && dotCounter >= 0){
                        img.setPixel(c1, c, pixelValue);
                    }
                    if(dotCounter >= dotSize){
                        dotCounter = -dotSize;
                    }
                    dotCounter++;
                }
            } else {
               for (int c = r1; c <= r2; c++) {
                    int r = (int) Math.round((c-q) / k);

                    if(dotCounter < dotSize && dotCounter >= 0){
                        img.setPixel(r,c, pixelValue);
                    }
                    if(dotCounter >= dotSize){
                        dotCounter = -dotSize;
                    }
                    dotCounter++;
                }
            }
        }
    }



}
