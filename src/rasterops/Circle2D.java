package rasterops;

import rasterdata.RasterImage;

public class Circle2D {
    public int x;
    public int y;
    public int radius;

    public Circle2D(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public Circle2D(int x, int y, int dx, int dy) {
        this.x = x;
        this.y = y;
        this.radius = (int) Math.sqrt(Math.pow(dx - x, 2) + Math.pow(dy - y, 2));
    }

    public void setRadius(int dx, int dy) {
        this.radius = (int) Math.sqrt(Math.pow(dx - x, 2) + Math.pow(dy - y, 2));
    }

    public void draw(RasterImage img, TrivialLiner liner) {
        for(double i = 0; i < Math.PI*2;i+=0.01) {
            img.setPixel((int) (x), (int) (y), 0x0000ff);
            img.setPixel((int) (x + Math.cos(i)*radius), (int) (y + Math.sin(i)*radius),0xffff00 );
        }
    }
}
