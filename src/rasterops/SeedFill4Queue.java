package rasterops;

import objectops.Point2D;
import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.function.Predicate;

public class SeedFill4Queue<P> implements SeedFill<P>{



    @Override
    public void fill(@NotNull RasterImage<P> image, int c, int r,
                     @NotNull P pixelValue, @NotNull Predicate<P> isInArea, boolean doPatternFill) {

        // Initialize an empty queue
        Deque<Point2D> queue = new ArrayDeque<>();
        // Insert seed into the queue
        Point2D seed = new Point2D(c, r);
        queue.add(seed);
        // While queue not empty:
        while(!queue.isEmpty()) {
            // Remove a point from the queue
            Point2D point = queue.poll();
            // If point is inside the image and inside the area:
            if(!image.getPixel(point.x, point.y).isEmpty() && isInArea.test(image.getPixel(point.x, point.y).get())) {
                if(doPatternFill) {
                    PatternFill patternFill = new ChessStyleFill();
                    image.setPixel(point.x, point.y, (P) patternFill.fill(point.x, point.y));
                } else {
                    // Set pixel to new value
                    image.setPixel(point.x, point.y, pixelValue);
                }
                // Add addresses of all neighboring pixels into queue
                queue.add(new Point2D(point.x + 1, point.y));
                queue.add(new Point2D(point.x - 1, point.y));
                queue.add(new Point2D(point.x, point.y + 1));
                queue.add(new Point2D(point.x, point.y - 1));
            }
        }

    }
}
