package rasterops;

import objectops.Point2D;
import rasterdata.RasterImage;

public class IsoscelesTriangle {
  private Line base = new Line(0, 0, 0, 0);
  private Point2D top;
  private boolean isBaseSet = false;
  private boolean finalState = false;

  private final Circler circler = new Circler();

  private Point2D midPoint = new Point2D(0,0);

    public IsoscelesTriangle(Line base, Point2D top) {
        this.base = base;
        this.top = top;
    }

    public IsoscelesTriangle() {
    }

    public IsoscelesTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        this.base = new Line(x1,y1,x2,y2);
    }

    public void draw(RasterImage img, TrivialLiner liner) {
        base.draw(img,liner, 0xff00ff);
        if(this.top != null) {
            Line l1 = new Line(base.x1, base.y1, top.x, top.y);
            Line l2 = new Line(base.x2, base.y2, top.x, top.y);
            l1.draw(img, liner, 0xff00ff);
            l2.draw(img, liner, 0xff00ff);
            img.setPixel(top.x, top.y, 0xffffff);

            if(!finalState) {
                liner.drawLineLined(img, midPoint.x, midPoint.y, top.x, top.y,10, 0xffff00);
                // calculate center between midpoint and top
                int cx = (top.x + midPoint.x) / 2;
                int cy = (top.y + midPoint.y) / 2;
                circler.drawCircle(img, cx, cy, (int) Math.sqrt(Math.pow(cx- midPoint.x,2) + Math.pow(cy - midPoint.y,2)));
            }
        }
    }

    public void setBase(int x1, int y1, int x2, int y2) {
        this.base = new Line(x1,y1,x2,y2);
    }

    public void setTop(Point2D top) {
        this.top = top;
    }

    public void calculateTop(Line base, Point2D mouse) {
        int x1 = base.x1;
        int y1 = base.y1;
        int x2 = base.x2;
        int y2 = base.y2;

        int midX = x1 + (x2 - x1) / 2;
        int midY = y1 + (y2 - y1) / 2;
        this.midPoint = new Point2D(midX, midY);

        double crossProduct = (mouse.x - midX) * (y2 - y1) - (mouse.y - midY) * (x2 - x1);
        // calculate distance between base and mouse
        double dist = Math.sqrt(Math.pow(mouse.x - midX, 2) + Math.pow(mouse.y - midY, 2));

        int xn = base.x2 - base.x1;
        int yn = base.y2 - base.y1;

        Vector2D p = new Vector2D(yn, -xn);

        if(crossProduct < 0) {
            p = new Vector2D(-yn, xn);
        }

        p.normalize();
        p.multiply(dist);

        p.x = p.x + midX;
        p.y = p.y + midY;

        this.top = p.toPoint2D();
    }


    public Point2D getTop() {
        return top;
    }

    public void setTop(int x, int y) {
        calculateTop(base, new Point2D(x,y));
    }

    public void setFinalState(boolean finalState) {
        this.finalState = finalState;
    }

    public boolean isBaseSet() {
        return isBaseSet;
    }

    public void setBaseSet(boolean baseSet) {
        isBaseSet = baseSet;
    }
}
