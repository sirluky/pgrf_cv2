package rasterops;

import objectdata.Edge;
import objectops.Point2D;
import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ScanLineImpl<P> implements ScanLine<P> {
    @Override
    public void fill(@NotNull RasterImage<P> img, @NotNull Polygon polygon, @NotNull P areaPixel, @NotNull TrivialLiner liner, @NotNull P polygonPixel, boolean doPatternFill) {
        final @NotNull List<Point2D> points = polygon.getVertices();
        @NotNull List<Edge> edges = new ArrayList<>();

        // fill the list with edges
        for(int i = 0; i < points.size(); i++){
            edges.add(new Edge(points.get(i), points.get((i+1)%points.size())));
        }

        // remove the horizontal line
        edges.removeIf(edge -> edge.start().y == edge.end().y);

        // orient all edges, and trying out Array.map()
        edges = edges.stream().map(edge -> edge.orientDown())
        // Sorting by X
                .sorted(Comparator.comparingInt(e -> e.start().x))
                .map(edge -> edge.shortened())
                .collect(Collectors.toList());

        // shortened edges
//        final @NotNull List<Edge> shortenedEdges = new ArrayList<>();
//
//        // shorten by one pixel
//        for(Edge edge : edges){
//            edge[].add(edge.shortened());
//        }
        // find yMin, yMax
        int yMin = Integer.MAX_VALUE;
        int yMax = Integer.MIN_VALUE;

        for(Edge edge : edges){
            if(edge.start().y < yMin){
                yMin = edge.start().y;
            }
            if(edge.end().y < yMin){
                yMin = edge.end().y;
            }
            if(edge.start().y > yMax){
                yMax = edge.start().y;
            }
            if(edge.end().y > yMax){
                yMax = edge.end().y;
            }
        }

        // for each y from yMin to yMax
           // for each edges
        for(int r = yMin; r < yMax; r++) {
            // initialize list of intersections

            final @NotNull List<Point2D> intersections = new ArrayList<>();
            for (Edge edge : edges) {
                // if y is in the range of the edge
                    // add the intersection to the list
                    if (edge.start().y == r) {
                        intersections.add(edge.start());
                    } else if (edge.end().y == r) {
                        intersections.add(edge.end());
                    } else {
                        if(edge.start().y < r && edge.end().y > r){
                            // detect edge intersect
                            intersections.add(new Point2D(edge.intersects(r),r));
                        }
                    }
            }

            // for each pair of intersections
            for (int i = 1; i < intersections.size(); i += 2) {
                // draw a line between the intersections
                if(doPatternFill) {
                    liner.drawLinePatterned(img, intersections.get(i - 1).x, intersections.get(i - 1).y, intersections.get(i).x, intersections.get(i).y);
                } else {
                    liner.drawLine(img, intersections.get(i-1).x, r, intersections.get(i).x, r, polygonPixel);
                }
            }
        }

    }
}
