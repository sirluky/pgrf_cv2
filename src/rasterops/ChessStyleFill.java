package rasterops;

public class ChessStyleFill implements PatternFill<Integer> {
    @Override
    public Integer fill(int x, int y) {
        if(x / 20 % 2 == y / 20 % 2) {
            return 0x000000;
        } else {
            return 0xffffff;
        }
    }
}
