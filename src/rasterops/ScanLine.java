package rasterops;

import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

public interface ScanLine<P> {
     public void fill(final @NotNull RasterImage<P> img, final @NotNull Polygon polygon, final @NotNull P areaPixel, final @NotNull TrivialLiner liner, final @NotNull P polygonPixel, boolean doPatternFill);

//     void fill(@NotNull RasterImage<P> img, @NotNull Polygon polygon, P areaPixel, @NotNull TrivialLiner liner, P polygonPixel, boolean doPatternFill);
}
