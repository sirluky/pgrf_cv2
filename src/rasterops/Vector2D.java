package rasterops;

import objectops.Point2D;

public class Vector2D  {

        public double x = 0;
        public double y = 0;

        public Vector2D(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public void normalize() {
            double length = Math.sqrt(x*x + y*y);
            x = x / length;
            y = y / length;
        }

        public void multiply(double mag) {
            x = x * mag;
            y = y * mag;
        }

        public Point2D toPoint2D() {
            return new Point2D((int)x, (int)y);
        }

}
