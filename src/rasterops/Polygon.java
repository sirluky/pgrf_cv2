package rasterops;
import objectdata.Edge;
import objectops.Point2D;
import org.jetbrains.annotations.NotNull;
import rasterdata.RasterImage;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Polygon<P> {
    private ArrayList<Point2D> vertices = new ArrayList<>();

    public Polygon() {

    }

    public void addPoint(int x, int y) {
        if(vertices.size() == 0) {
            vertices.add(new Point2D(x,y));
        }
        vertices.add(new Point2D(x,y));
    }

    public void addPoints(List<Point2D> points) {
        vertices.addAll(points);
    }

    public void updateLastPoint(int x, int y) {
        vertices.get(vertices.size()-1).x = x;
        vertices.get(vertices.size()-1).y = y;
    }

    public void close() {
        vertices.add(new Point2D(vertices.get(0).x,vertices.get(0).y));
    }


    public ArrayList<Point2D> getVertices() {
        return vertices;
    }

    public void draw(RasterImage img, TrivialLiner liner, P pixelValue) {
        if(vertices.size() > 1) {
            for(int i = 1; i < vertices.size(); i++) {
                liner.drawLine(img,vertices.get(i-1).x, vertices.get(i-1).y, vertices.get(i).x, vertices.get(i).y, pixelValue);
            }
            int lastI = vertices.size()-1;
            liner.drawLine(img,vertices.get(0).x, vertices.get(0).y, vertices.get(lastI).x, vertices.get(lastI).y, pixelValue);
        }
    }

    public List<Edge> getEdges() {
        @NotNull List<Edge> edges = new ArrayList<>();

        // fill the list with edges
        for(int i = 0; i < vertices.size(); i++){
            edges.add(new Edge(vertices.get(i), vertices.get((i+1)%vertices.size())));
        }
        return edges;
    }

    public boolean isPointInsidePolygon(Point2D point2D) {
        boolean left = false;
        return true;
    }

    public List<Polygon> calculateIntersectingPolygons(Polygon polygon2) {
        ArrayList<Polygon> out = new ArrayList();
        List<Edge> polygon1Edges = this.getEdges();
        List<Edge> polygon2Edges = polygon2.getEdges();

        ArrayList<Point2D> intersections = new ArrayList<>();

        Polygon polygon = new Polygon();
        for(Edge edge : polygon1Edges) {
            for(Edge edge2 : polygon2Edges) {
                Optional<Point2D> intersection = edge.intersectsAt(edge2);
                if(intersection.isPresent()) {
                    intersections.add(intersection.get());
                }
            }
        }



        for(Point2D intersection : intersections) {
            polygon.addPoint(intersection.x, intersection.y);
        }


        ArrayList<Point2D> polygon2Vertices = polygon2.getVertices();
        for(Point2D point : polygon2Vertices) {
            // raycast from left to right until we hit the point anything over is discarded
            int intersectsTimes = (int) polygon1Edges.stream().filter(edge -> {
                Optional<Integer> at = edge.intersectsXAt(point.y);
                return at.isPresent() && at.get() <= point.x;
            }).count();
            // if the raycast intersects odd number of times then the point is inside
//        return intersectsTimes % 2 != 0;
            if(intersectsTimes % 2 != 0) {
                polygon.addPoint(point.x, point.y);
            }
        }


        // Same but for inner points
        for(Point2D point : this.getVertices()) {
            int intersectsTimes = (int) polygon2Edges.stream().filter(edge -> {
                Optional<Integer> at = edge.intersectsXAt(point.y);
                return at.isPresent() && at.get() <= point.x;
            }).count();
            if(intersectsTimes % 2 != 0) {
                polygon.addPoint(point.x, point.y);
            }
        }

//        List<Point2D> polygon1Vertices = getEdgesInsideIntersection(polygon1Edges, polygon.getVertices());
//        polygon.addPoints(polygon1Vertices);
//
//        List<Point2D> polygon2VerticesInside = getEdgesInsideIntersection(polygon2Edges, polygon2.getVertices());
//        polygon.addPoints(polygon2VerticesInside);


        polygon = orderPolygonVerticesClockwise(polygon);
        ArrayList<Polygon> p = new ArrayList<>();
        p.add(polygon);

        return p;
    }

    @Override
    public String toString() {
        String verticesString = "";

        for(Point2D p : vertices){
            verticesString += (p.toString() + ", ");
        }

        return "Polygon{" +
                "vertices=" + verticesString +
                '}';
    }

    public Polygon orderPolygonVerticesClockwise(Polygon polygon) {
        double mX = 0;
        double my = 0;
        ArrayList<Point2D> points = polygon.getVertices();
        for (Point2D p : points)
        {
            mX += p.x;
            my += p.y;
        }
        mX /= points.size();
        my /= points.size();
        double finalMy = my;
        double finalMX = mX;

        List sortedPoints =  points.stream().sorted(Comparator.comparingDouble(v -> Math.atan2(v.y - finalMy, v.x - finalMX)))
                .collect(Collectors.toList());

        Polygon out = new Polygon();
        for(Object p : sortedPoints) {
            out.addPoint(((Point2D) p).x, ((Point2D) p).y);
        }
        return out;
    }

    private List<Point2D> getEdgesInsideIntersection(List<Edge> edges, List<Point2D> points) {
        ArrayList<Point2D> newPoints = new ArrayList<>();
        //  add vertexes which are included in cropping polygon area
        for(Point2D point : points) {
            // raycast left to right until we hit the point anything over is discarded
            int intersectsTimes = (int) edges.stream().filter(edge -> {
                Optional<Integer> at = edge.intersectsXAt(point.y);
                return at.isPresent() && at.get() <= point.x;
            }).count();
            if(intersectsTimes % 2 != 0) {
                newPoints.add(new Point2D(point.x, point.y));
            }
        }
        return newPoints;
    }
}
