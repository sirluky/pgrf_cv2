package rasterops;

import objectops.Point2D;

public interface PatternFill<P> {
    P fill(int x, int y);

    default P fill(Point2D point) {
        return fill(point.x, point.y);
    }
}
