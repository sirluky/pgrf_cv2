package rasterops;

import rasterdata.RasterImage;

public class Circler {

    public void drawCircle(RasterImage img, int x, int y, int radius) {
        for(double i = 0; i < Math.PI*2;i+=0.01) {
            img.setPixel((int) (x), (int) (y), 	0xFFA500);
            img.setPixel((int) (x + Math.cos(i)*radius), (int) (y + Math.sin(i)*radius),	0xFFA500 );
        }
    }

}
