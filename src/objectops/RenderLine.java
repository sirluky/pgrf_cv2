package objectops;

import objectdata.Scene;
import objectdata.Solid;
import rasterdata.RasterImage;
import rasterops.Liner;
import rasterops.PatternFill;
import transforms.Mat4;

public interface RenderLine {
    void renderScene(Scene scene, Mat4 viewMat, Mat4 projectionMat, PatternFill color, Liner liner, RasterImage raster);

    void renderSolid(Solid solid, Mat4 transMat, PatternFill color, Liner liner, RasterImage raster);

    void setSelectedObjectIndex(int index);
}
