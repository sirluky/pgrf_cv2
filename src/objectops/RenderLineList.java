package objectops;

import objectdata.Scene;
import objectdata.Solid;
import rasterdata.RasterImage;
import rasterops.Liner;
import transforms.*;

import java.util.List;
import java.util.stream.Collectors;

public class RenderLineList<P> {


    RasterImage img = null;
    Liner liner = null;
    Camera camera = null;

    public int selectedObjectIndex = 7;
    private P pixelValueSelected = null;

//    PatternFill color = null;

    public RenderLineList(Camera camera,RasterImage img, Liner liner) {
        this.img = img;
        this.liner = liner;
        this.camera = camera;
//        this.color = color;
    }

    //    @Override
    public void renderScene(Scene scene, Mat4 viewMat, Mat4 projectionMat, Liner liner, RasterImage img, P pixelValue, P pixelValueSelected) {
        List<Solid> solids = scene.getSolids();
        List<Mat4> modelMat = scene.getModelMats();
        this.pixelValueSelected = pixelValueSelected;
        // foreach solid
        for (int i = 0; i < solids.size(); i++) {
            // create trans matrix (modelMat * viewMat * projMat)
            Mat4 transMat = modelMat.get(i).mul(viewMat).mul(projectionMat);
            // render solid
            if (selectedObjectIndex == i || selectedObjectIndex < 0) {
                renderSolid(solids.get(i), transMat, liner, img, pixelValueSelected);
            } else {
                renderSolid(solids.get(i), transMat, liner, img, pixelValue);
            }
        }
    }

    private Vec3D transformToCenter(Vec3D point) {
        return point.mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(new Vec3D(img.getWidth() / 2, img.getHeight() / 2, 1));
    }

//    @Override
public void renderSolid(Solid solid, Mat4 transMat, Liner liner, RasterImage img, P pixelValue) {
    // foreach vertex
    List<Point3D> vertices = solid.getVertices().stream()
            // multiply by transM
            .map(point3D -> point3D.mul(transMat))
            .collect(Collectors.toList());

    List<Integer> indices = solid.getIndices();
    // for each line segment
    for (int i = 0; i < indices.size(); i += 2) {
        Point3D start = vertices.get(indices.get(i));
        Point3D end = vertices.get(indices.get(i + 1));

        // discard only lines that are out
        if (!isInViewSpace(start) && !isInViewSpace(end)) continue;

        int finalI = i;
        start.dehomog().ifPresent(startPoint -> end.dehomog().ifPresent(endPoint -> {
            // draw lines with liner
            P pixel = pixelValue;
            if(!solid.getPixel(finalI).isEmpty() && pixelValueSelected != pixel) {
                pixel = (P) solid.getPixel(finalI).get();
            }
            liner.drawLine(img, transformToViewport(startPoint, img), transformToViewport(endPoint, img), pixel);
        }));
    }

    }

    private boolean isInViewSpace(Point3D point) {
        double w = point.getW();
        return point.getX() <= w && point.getX() >= -w &&
                point.getY() <= w && point.getY() >= -w &&
                point.getZ() <= w && point.getZ() >= 0;
    }

    private transforms.Point2D transformToViewport(Vec3D point, RasterImage img) {
        return new transforms.Point2D(
                (int) Math.round((point.getX() + 1) / 2 * (img.getWidth() - 1)),
                (int) Math.round((1 - (point.getY() + 1) / 2) * (img.getHeight() - 1))
        );
    }

    public void setSelectedObjectIndex(int selectedObjectIndex) {
        this.selectedObjectIndex = selectedObjectIndex;
    }
}
