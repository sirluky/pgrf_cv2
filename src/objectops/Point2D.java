package objectops;

public class Point2D {
    public int x, y;

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int hashCode() {
        return x * 55 + y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point2D) {
            Point2D point = (Point2D) obj;
            return this.getX() == point.getX() && this.getY() == point.getY();
        }
        return false;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ']';
    }

    public transforms.Point2D toTransformsPoint2D() {
        return new transforms.Point2D(x, y);
    }

    public static Point2D fromTransformsPoint2D(transforms.Point2D point) {
        return new Point2D((int) Math.round(point.getX()), (int) Math.round(point.getY()));
    }
}
