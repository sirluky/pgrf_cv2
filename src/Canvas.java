//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;

import rasterdata.Presentable;
import rasterdata.RasterImage;
import rasterdata.RasterImageBI;
import rasterops.*;
import rasterops.Polygon;
import transforms.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2020
 */

public class Canvas {

    private JFrame frame;
    private JPanel panel;
    private final RasterImage<Integer> img;
    private final Presentable<Graphics> presenter;
    private final TrivialLiner<Integer> liner;
    private int c1, r1, c2, r2;

    Vec3D cameraPosition = new Vec3D(2, 4, 3);
    Camera camera = new Camera()
            .withPosition(cameraPosition)
            .withAzimuth(azimuthToOrigin(cameraPosition))
            .withZenith(zenithToOrigin((cameraPosition)));

    private enum shapeModeEnum {
        POLYGON,
        POLYGON_2,
        LINE,
        CIRCLE,
        TRIANGLE,

        SEED_FILL4
    }

    private shapeModeEnum shapeMode = shapeModeEnum.POLYGON_2;

    private boolean isChessStyleFill = false;

    private ArrayList<Line> lines = new ArrayList<>();
    private Circle2D circle = new Circle2D(0, 0, 0);
    private Polygon polygon = new Polygon();
    private Polygon polygon2 = new Polygon();
    private IsoscelesTriangle triangle = new IsoscelesTriangle();

    private char rotateMode = 'Z';
//	public static enum Rotation {
//			X, Y, Z
//	}


    private ScanLine scanLineFill = new ScanLineImpl();

    private Point2D sf4Seed = null;
    private final double CAM_SPEED = 0.05;

    private World3D world3D;


    public Canvas(int width, int height) {
        frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("UHK FIM PGRF Kovar Lukas : " + this.getClass().getName());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(new TextField("Kamera pohyb: WSAD. Nahoru E, Dolu Q. Vybrat objekty: F1-F6. Vše F10."), BorderLayout.NORTH);
        frame.add(new TextField("Rotace N,M,J,K. Pohyb s objekty šipkama + U,I nahoru/dolu. Zoom/zvětšení objektu C,V"), BorderLayout.SOUTH);


        final RasterImageBI auxRasterImageBI = new RasterImageBI(width, height);
        img = auxRasterImageBI;
        presenter = auxRasterImageBI;
        liner = new TrivialLiner<>();


        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };

        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (shapeMode == shapeModeEnum.LINE) {
                    draw();

                    liner.drawLineLined(img, c1, r1, e.getX(), e.getY(), 10, 0x0000ff);
                }

                if (shapeMode == shapeModeEnum.CIRCLE) {
                    draw();

                    circle.setRadius(e.getX(), e.getY());
                }

                if (shapeMode == shapeModeEnum.TRIANGLE) {
                    if (!triangle.isBaseSet()) {
                        triangle.setBase(c1, r1, e.getX(), e.getY());
                    } else {
                        triangle.setTop(e.getX(), e.getY());
                    }
                    draw();
                }

                if (shapeMode == shapeModeEnum.POLYGON) {
                    polygon.updateLastPoint(e.getX(), e.getY());
                    draw();
                }

                if (shapeMode == shapeModeEnum.POLYGON_2) {
                    polygon2.updateLastPoint(e.getX(), e.getY());
                    draw();
                }

                present();
            }
        });

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                c1 = e.getX();
                r1 = e.getY();

                if (shapeMode == shapeModeEnum.POLYGON) {
                    polygon.addPoint(e.getX(), e.getY());
                }

                if (shapeMode == shapeModeEnum.POLYGON_2) {
                    polygon2.addPoint(e.getX(), e.getY());
                }

                if (shapeMode == shapeModeEnum.CIRCLE) {
                    circle = new Circle2D(e.getX(), e.getY(), 0);
                }
                if (shapeMode == shapeModeEnum.TRIANGLE) {
                    triangle.setFinalState(false);
                }
                draw();
                if (shapeMode == shapeModeEnum.SEED_FILL4) {
                    sf4Seed = new Point2D(e.getX(), e.getY());
                }
                present();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                c2 = e.getX();
                r2 = e.getY();

                if (shapeMode == shapeModeEnum.LINE) {
                    lines.add(new Line(c1, r1, c2, r2));
                }

                if (shapeMode == shapeModeEnum.TRIANGLE) {
                    if (!triangle.isBaseSet()) {
                        triangle.setBase(c1, r1, c2, r2);
                        triangle.setBaseSet(true);
                    } else {
                        triangle.setFinalState(true);
                    }
                }
                draw();
                present();
            }

        });
        final Point2D[] mouseLook = {null, null};
        panel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                mouseLook[1] = new Point2D(e.getX(), e.getY());

                world3D.mouseMove(mouseLook);
            }
        });

        final double[] mouseWheelMoved = {0};
        panel.addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                mouseWheelMoved[0] += e.getPreciseWheelRotation();
            }
        });

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mouseLook[0] = new Point2D(e.getX(), e.getY());
            }
        });

        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

        panel.requestFocus();
        panel.requestFocusInWindow();
        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {

                    // Move in Space
                    case KeyEvent.VK_W:
                        world3D.move('W');
                        break;

                    case KeyEvent.VK_A:
                        world3D.move('A');
                        break;


                    case KeyEvent.VK_D:
                        world3D.move('D');
                        break;

                    case KeyEvent.VK_S:
                        world3D.move('S');
                        break;

                    case KeyEvent.VK_Q:
                        world3D.move('Q');
                        break;

                    case KeyEvent.VK_E:
                        world3D.move('E');
                        break;


                    // Model transformation in Space
                    case KeyEvent.VK_N:
                        world3D.rotateObjects('Z', 0.3);
                        break;

                    case KeyEvent.VK_M:
                        world3D.rotateObjects('Z', -0.3);
                        break;

                    // Model transformation in Space
                    case KeyEvent.VK_J:
                        world3D.rotateObjects('Y', 0.3);
                        break;

                    case KeyEvent.VK_K:
                        world3D.rotateObjects('Y', -0.3);
                        break;

                    case KeyEvent.VK_UP:
                        world3D.moveObjects('Z', -0.3);
                        break;

                    case KeyEvent.VK_DOWN:
                        world3D.moveObjects('Z', 0.3);
                        break;

                    case KeyEvent.VK_RIGHT:
                        world3D.moveObjects('X', -0.3);
                        break;

                    case KeyEvent.VK_LEFT:
                        world3D.moveObjects('X', 0.3);
                        break;

                    case KeyEvent.VK_U:
                        world3D.moveObjects('Y', 0.3);
                        break;

                    case KeyEvent.VK_I:
                        world3D.moveObjects('Y', -0.3);
                        break;


                    // Object select
                    case KeyEvent.VK_F1:
                        world3D.setSelectedObjectIndex(1);
                        break;
                    case KeyEvent.VK_F2:
                        world3D.setSelectedObjectIndex(2);
                        break;
                    case KeyEvent.VK_F3:
                        world3D.setSelectedObjectIndex(3);
                        break;
                    case KeyEvent.VK_F4:
                        world3D.setSelectedObjectIndex(4);
                        break;
                    case KeyEvent.VK_F5:
                        world3D.setSelectedObjectIndex(5);
                        break;
                    case KeyEvent.VK_F6:
                        world3D.setSelectedObjectIndex(6);
                        break;
                    case KeyEvent.VK_F10:
                        world3D.setSelectedObjectIndex(-1);
                        break;

                    case KeyEvent.VK_C:
                        world3D.zoomObjects(1.1);
                        break;
                    case KeyEvent.VK_V:
                        world3D.zoomObjects(0.9);
                        break;

                }

                draw();
                panel.repaint();
            }
        });

        this.world3D = new World3D(img, liner, this, panel);
    }

    public void draw() {
//		clear();


    }

    public double azimuthToOrigin(Vec3D pos) {
        final Vec3D v = pos.opposite();

        double a = v.ignoreZ().normalized()
                .map(vNorm -> Math.acos(vNorm.dot(new Vec2D(1, 0))))
                .orElse(0.0);

        return v.getY() > 0 ? a : 2 * Math.PI - a;
    }

    public double zenithToOrigin(Vec3D pos) {
        final Vec3D v = pos.opposite();

        return v.normalized()
                .map(vNorm -> Math.PI / 2 - Math.acos(vNorm.dot(new Vec3D(0, 0, 1))))
                .orElse(Math.PI / 2);
    }

    public void clear() {
        img.clear(0x2f2f2f);
    }

    public void present(final Graphics graphics) {
        presenter.present(graphics);
    }

    public void present() {
        final Graphics g = panel.getGraphics();
        if (g != null) {
            presenter.present(g);
        }
    }

    public void start() {
        draw();
        present();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Canvas(800, 600).start());
    }


}