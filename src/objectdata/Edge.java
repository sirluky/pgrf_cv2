package objectdata;

import objectops.Point2D;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public record Edge(@NotNull Point2D start, @NotNull Point2D end) {

    public Edge orientDown() {
        return (start.y <= end.y) ? this : new Edge(end, start);
    }

    public @NotNull Edge shortened() {
        double k;
        if (start.x != end.x)
            k = (end.y - start.y) / (double) (end.x - start.x);
        else
            k = Integer.MAX_VALUE;
        final double q = start.y - k * start.x;

        int shortenBy = 0;
        if (start.y != end.y)
            shortenBy = (start.y > end.y) ? -1 : 1;
        int y = end.y - shortenBy;
        int x = (int) Math.round((y - q) / k);

        return new Edge(start, new Point2D(x, y));
    }

    public Integer intersects(int y) {
        double k;
        if (start.x != end.x)
            k = (end.y - start.y) / (double) (end.x - start.x);
        else
            k = Integer.MAX_VALUE;
        double q = start.y - k * start.x;

        double x = (y - q) / k;

        return (int) Math.round(x);
    }

    public boolean intersectsY(int y) {
        return Integer.min(start.y, end.y) <= y && Integer.max(start.y, end.y) >= y;
    }


    public Optional<Integer> intersectsXAt(int y) {
        if (!intersectsY(y)) return Optional.empty();
        double k;
        if (start.x != end.x)
            k = (end.y - start.y) / (double) (end.x - start.x);
        else
            k = Integer.MAX_VALUE;
        double q = start.y - k * start.x;

        double x = (y - q) / k;

        return Optional.of((int) Math.round(x));
    }

    public Optional<Point2D> intersectsAt(Edge other) {
        double kCurrent;
        if (start.x != end.x)
            kCurrent = (end.y - start.y) / (double) (end.x - start.x);
        else
            kCurrent = Integer.MAX_VALUE;
        final double qCurr = start.y - kCurrent * start.x;

        double kSecond;
        if (other.start().x != other.end().x)
            kSecond = (other.end().y - other.start().y) / (double) (other.end().x - other.start().x);
        else
            kSecond = Integer.MAX_VALUE;
        final double qOther = other.start().y - kSecond * other.start().x;

        // calc intersection
        // y = kCurr  * x + qCurr
        // y = kOther * x + qOther

        // check if x is in range
        double x = (qOther - qCurr) / (kCurrent - kSecond);
        int minXCurr = Integer.min(start.x, end.x);
        int maxXCurr = Integer.max(start.x, end.x);
        int minXOther = Integer.min(other.start().x, other.end().x);
        int maxXOther = Integer.max(other.start().x, other.end().x);
        if (x > maxXCurr || x > maxXOther || x < minXCurr || x < minXOther) return Optional.empty();

        // check if y is in range
        double y = kCurrent * x + qCurr;
        int minYCurr = Integer.min(start.y, end.y);
        int maxYCurr = Integer.max(start.y, end.y);
        int minYOther = Integer.min(other.start().y, other.end().y);
        int maxYOther = Integer.max(other.start().y, other.end().y);
        if (y > maxYCurr || y > maxYOther || y < minYCurr || y < minYOther) return Optional.empty();

        return Optional.of(new Point2D((int) Math.round(x), (int) Math.round(y)));
    }

    public Point2D getStart() {
        return start;
    }

    public Point2D getEnd() {
        return end;
    }
}
