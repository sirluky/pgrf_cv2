package objectdata;

import transforms.Point3D;

import java.util.*;
import java.util.stream.Collectors;

public class Pyramid implements Solid {
    private final List<Point3D> vertices;
    private final List<Integer> indices;

    public HashMap<Integer, Integer> pixelBuffer = new HashMap<>();

    public Pyramid() {
        this.vertices = new ArrayList<>(5);

        // Pyramid rect
        vertices.add(new Point3D(0, 0, 0));
        vertices.add(new Point3D(1, 0, 0));
        vertices.add(new Point3D(1, 1, 0));
        vertices.add(new Point3D(0, 1, 0));
        vertices.add(new Point3D(0.5, 0.5, 1));

        int[] indices = {
                0, 1, 1, 2, 2, 3, 3, 0, 4, 0, 4, 1, 4, 2, 4, 3
        };

        for (int i = 0; i < indices.length; i++) {
            addPixel(i, 0xffff00);
        }

        this.indices = Arrays.stream(indices).boxed().collect(Collectors.toList());
    }

    @Override
    public List<Point3D> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public Optional<Integer> getPixel(Integer key) {
        if(pixelBuffer.containsKey(key)) {
            return Optional.of(pixelBuffer.get(key));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void addPixel(int index, int color) {
        pixelBuffer.put(index, color);
    }
}
