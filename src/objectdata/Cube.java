package objectdata;

import transforms.Point3D;

import java.util.*;
import java.util.stream.Collectors;

public class Cube implements Solid {
    private final List<Point3D> vertices;
    private final List<Integer> indices;

    public HashMap<Integer, Integer> pixelBuffer = new HashMap<>();

    public Cube() {
        this.vertices = new ArrayList<>(8);

        // bottom rectangle
        vertices.add(new Point3D(0, 0, 0)); // 0
        vertices.add(new Point3D(0, 1, 0)); // 1
        vertices.add(new Point3D(1, 1, 0)); // 2
        vertices.add(new Point3D(1, 0, 0)); // 3

        // top rectangle
        vertices.add(new Point3D(0, 0, 1)); // 4
        vertices.add(new Point3D(0, 1, 1)); // 5
        vertices.add(new Point3D(1, 1, 1)); // 6
        vertices.add(new Point3D(1, 0, 1)); // 7

        int[] indices = {
                // connect bottom rectangle
                0, 1,
                1, 2,
                2, 3,
                3, 0,
                // connect top rectangle
                4, 5,
                5, 6,
                6, 7,
                7, 4,
                // connect bottom and top rectangle
                0, 4,
                1, 5,
                2, 6,
                3, 7
        };
        this.indices = Arrays.stream(indices).boxed().collect(Collectors.toList());
    }

    @Override
    public List<Point3D> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }

    @Override
    public Optional<Integer> getPixel(Integer key) {
        if(pixelBuffer.containsKey(key)) {
            return Optional.of(pixelBuffer.get(key));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void addPixel(int index, int color) {
        pixelBuffer.put(index, color);
    }
}
