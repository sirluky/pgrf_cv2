package objectdata;

import objectops.Point2D;
import org.jetbrains.annotations.NotNull;
import transforms.Mat3;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Polygon2D {

    private final List<Point2D> points = new ArrayList<>();

    public Polygon2D() {
    }

    public Polygon2D(List<Point2D> points) {
        this.points.addAll(points);
    }

    public void addPoint(Point2D point){
        points.add(point);
    }

    public Point2D getPoint(int index){
        return points.get(index);
    }

    public List<Point2D> getPoints(){
        return points;
    }

    public int getNumberOfPoints(){
        return points.size();
    }

    public List<Edge> getEdges() {
        @NotNull List<Edge> edges = new ArrayList<>();

        // fill the list with edges
        for(int i = 0; i < points.size(); i++){
            edges.add(new Edge(points.get(i), points.get((i+1)%points.size())));
        }
        return edges;
    }


//    public Polygon2D transform(Mat3 mat) {
////        List<Point2D> transformedPoints = points.stream().map(p -> p.mul(mat)).toList();
//    }
}
