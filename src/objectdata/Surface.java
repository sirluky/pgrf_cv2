package objectdata;

import rasterdata.RasterImage;
import rasterops.Liner;
import rasterops.PatternFill;
import transforms.Bicubic;
import transforms.Mat4;
import transforms.Point3D;

import java.util.*;
import java.util.stream.Collectors;

public class Surface implements Solid {
    private final List<Point3D> vertices;
    private final List<Integer> indices;
    public HashMap<Integer, Integer> pixelBuffer = new HashMap<>();


    public Surface(Mat4 cubicMat, Point3D[] points, int numSecs) {
        this.indices = new ArrayList<>();
        double step = 1.0 / numSecs;

        // Create a bicubic surface using the base matrix and the control points
        Bicubic bicubic = new Bicubic(cubicMat, points);

        // Create a list to store the points on the bicubic surface
        List<List<Point3D>> surfacePoints = new ArrayList<>();

        // Iterate over the u and v parameters from 0 to 1
        for (double u = 0; u <= 1; u += step) {
            List<Point3D> row = new ArrayList<>();
            for (double v = 0; v <= 1; v += step) {
                // Compute the coordinates of a point on the bicubic surface
                Point3D point = bicubic.compute(u, v);
                row.add(point);
            }
            surfacePoints.add(row);
        }

        // Iterate over all pairs of adjacent points on the bicubic surface in the u-axis
        for (int u = 0; u < surfacePoints.size() - 1; u++) {
            for (int v = 0; v < surfacePoints.get(0).size(); v++) {
                indices.add(surfacePoints.get(0).size() * u + v);
                indices.add(surfacePoints.get(0).size() * (u + 1) + v);
            }
        }

        // Iterate over all pairs of adjacent points on the bicubic surface in the v-axis
        for (int u = 0; u < surfacePoints.size(); u++) {
            for (int v = 0; v < surfacePoints.get(0).size() - 1; v++) {
                indices.add(surfacePoints.get(0).size() * u + v);
                indices.add(surfacePoints.get(0).size() * u + v + 1);
            }
        }

        this.vertices = surfacePoints.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Override
    public List<Point3D> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }


    @Override
    public Optional<Integer> getPixel(Integer key) {
        if (pixelBuffer.containsKey(key)) {
            return Optional.of(pixelBuffer.get(key));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void addPixel(int index, int color) {
        pixelBuffer.put(index, color);
    }

}
