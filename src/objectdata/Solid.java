package objectdata;

import transforms.Point3D;

import java.util.List;
import java.util.Optional;

public interface Solid {

     List<Point3D> getVertices();
     List<Integer> getIndices();

     Optional<Integer> getPixel(Integer key);

     void addPixel(int index, int color);
}
