package objectdata;

import objectdata.Solid;
import transforms.Point3D;

import java.util.*;
import java.util.stream.Collectors;

public class Axis implements Solid {
    private final List<Point3D> vertices;
    private final List<Integer> indices;

    public HashMap<Integer, Integer> pixelBuffer = new HashMap<>();


    public Axis() {
        this.vertices = new ArrayList<>(8);

        // Axis
        this.vertices.add(new Point3D(0, 0, 0));
        this.vertices.add(new Point3D(5, 0, 0));
        this.vertices.add(new Point3D(0, 5, 0));
        this.vertices.add(new Point3D(0, 0, 5));

        int[] indicesArray = {
                0, 1, 0, 2, 0, 3
        };
        this.indices = Arrays.stream(indicesArray).boxed().collect(Collectors.toList());
        addPixel(0,0x0000ff);
        addPixel(2,0xff0000);
        addPixel(4,0x00ff00);

    }

    @Override
    public List<Point3D> getVertices() {
        return vertices;
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }


    @Override
    public Optional<Integer> getPixel(Integer key) {
        if(pixelBuffer.containsKey(key)) {
            return Optional.of(pixelBuffer.get(key));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void addPixel(int index, int color) {
        pixelBuffer.put(index, color);
    }
}